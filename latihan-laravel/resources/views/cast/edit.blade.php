@extends('layout.master')
   

    @section('title')
    Edit id:{{$post->id}}
        
    @endsection

    @section('content')


<div>
    <h2>Edit Post {{$post->id}}</h2>
    <form action="/cast/{{$post->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$post->nama}}"  placeholder="Masukkan Title">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="text" class="form-control" name="umur"  value="{{$post->umur}}"  placeholder="Masukkan Body">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" name="bio"  value="{{$post->bio}}" placeholder="Masukkan Body">
            @error('body')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@endsection