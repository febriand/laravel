@extends('layout.master')
   

    @section('title')
    Tambah Cast
        
    @endsection

    @section('content')


<div>
   
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama">
                
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur"  placeholder="Masukkan Umur">
                
                <label for="title">Bio</label>
                <input type="text" class="form-control" name="bio"  placeholder="Masukkan Biodata">
                
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection