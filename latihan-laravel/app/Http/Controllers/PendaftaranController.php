<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PendaftaranController extends Controller
{
    public function daftar(){
        return view('page.form');
    }

    public function submit(Request $request){
    $first=$request['first'];
    $last=$request['last'];
    return view('page.selamat',compact('first','last'));
    }
}
